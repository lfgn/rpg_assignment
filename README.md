# RPG Assignment
 
> Java program that simulates behavior of an RPG game. The simulation handles the interaction and relation of different characters and their features with external events such as character's levelling and equipment addition ( "Weapons" and "Armor" ). When reaching a new level and/or adding equipment to a character, the character's and equipment's statistics change accordingly.

## Program functional features
> * Abstract Classes and Classes
> * Enumerated Types
> * Customized Exceptions
> * Full Function Test Coverage
## RPG features : 
* ### Characters
 > - Mage  
 > - Ranger
 > - Rogue
 > - Warrior

* ### Character's Attributes
 #### Primary Attributes
> - Vitality
> - Strength
> - Dexterity
> - Intelligence
 #### Secondary Attributes
> - Health
> - Armor Rating
> - Elemental Resistance

* ### Weapons
> - Axe  
> - Bow
> - Dagger
> - Hammer
> - Staff
> - Sword
> - Wand

* ### Armor
> - Cloth
> - Leather
> - Mail
> - Plate


