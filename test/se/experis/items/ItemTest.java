package se.experis.items;

import org.junit.jupiter.api.Test;
import se.experis.attributes.PrimaryAttributes;
import se.experis.characters.Warrior;
import se.experis.customExceptions.InvalidArmorException;
import se.experis.customExceptions.InvalidWeaponException;
import se.experis.items.types.ArmorType;
import se.experis.items.types.Slot;
import se.experis.items.types.WeaponType;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    void warrior_triesToEquip_with_highLevelWeapon_throwsException_isTrue(){
        Warrior testWarrior = new Warrior("testWarrior");

        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setLevel(2);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        assertThrows(InvalidWeaponException.class, ()-> testWarrior.equipWeapon(testWeapon));
    }

    @Test
    void warrior_triesToEquip_with_highLevelArmor_throwsException_isTrue(){
        Warrior testWarrior = new Warrior("testWarrior");

        Armor testArmor = new Armor();
        testArmor.setName("Common Plate Body Armor");
        testArmor.setLevel(2);
        testArmor.setSlot(Slot.BODY);
        testArmor.setType(ArmorType.PLATE);
        testArmor.setArmorPrimaryAttributes(new PrimaryAttributes(2,1,0,0));

        assertThrows(InvalidArmorException.class, ()-> testWarrior.equipArmor(testArmor));
    }

    @Test
    void warrior_triesToEquip_with_wrongWeaponType_throwsException_isTrue(){
        Warrior testWarrior = new Warrior("testWarrior");

        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Bow");
        testWeapon.setLevel(1);
        testWeapon.setType(WeaponType.BOW);
        testWeapon.setDamage(12);
        testWeapon.setAttackSpeed(0.8);

        assertThrows(InvalidWeaponException.class, ()-> testWarrior.equipWeapon(testWeapon));
    }

    @Test
    void warrior_triesToEquip_with_wrongArmorType_throwsException_isTrue(){
        Warrior testWarrior = new Warrior("testWarrior");

        Armor testArmor = new Armor();
        testArmor.setName("Common Cloth Head Armor");
        testArmor.setLevel(1);
        testArmor.setSlot(Slot.HEAD);
        testArmor.setType(ArmorType.CLOTH);
        testArmor.setArmorPrimaryAttributes(new PrimaryAttributes(1,0,0,5));

        assertThrows(InvalidArmorException.class, ()-> testWarrior.equipArmor(testArmor));
    }

    @Test
    void warrior_equips_with_validWeapon_isTrue() throws InvalidWeaponException {
        Warrior testWarrior = new Warrior("testWarrior");

        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        testWarrior.equipWeapon(testWeapon);

        assertTrue(testWarrior.getEquipment().containsKey(Slot.WEAPON));
    }

    @Test
    void warrior_equips_with_validArmor_isTrue() throws  InvalidArmorException {
        Warrior testWarrior = new Warrior("testWarrior");

        Armor testArmor = new Armor();
        testArmor.setName("Common Plate Body Armor");
        testArmor.setLevel(1);
        testArmor.setSlot(Slot.BODY);
        testArmor.setType(ArmorType.PLATE);
        testArmor.setArmorPrimaryAttributes(new PrimaryAttributes(2,1,0,0));

        testWarrior.equipArmor(testArmor);

        assertTrue(testWarrior.getEquipment().containsKey(Slot.BODY));
    }

    @Test
    void calculate_warriorDPS_withNoWeapon_equipped(){
        Warrior testWarrior = new Warrior("testWarrior");
        double expectedDPS = 1*(1 + (5 / 100));
        assertEquals(expectedDPS, testWarrior.getDPS());
    }

    @Test
    void calculate_warriorDPS_withValidWeapon_equipped() throws InvalidWeaponException {
        Warrior testWarrior = new Warrior("testWarrior");

        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        testWeapon.setDPS();

        testWarrior.equipWeapon(testWeapon);
        double expectedDPS = (7 * 1.1)*(1 + (5 / 100));

        assertEquals(expectedDPS, testWarrior.getDPS());
    }

    @Test
    void calculate_warriorDPS_withValidWeapon_and_withValidArmor_equipped() throws InvalidWeaponException, InvalidArmorException {
        Warrior testWarrior = new Warrior("testWarrior");

        Weapon testWeapon = new Weapon();
        testWeapon.setName("Common Axe");
        testWeapon.setLevel(1);
        testWeapon.setSlot(Slot.WEAPON);
        testWeapon.setType(WeaponType.AXE);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);
        testWeapon.setDPS();

        Armor testArmor = new Armor();
        testArmor.setName("Common Plate Body Armor");
        testArmor.setLevel(1);
        testArmor.setSlot(Slot.BODY);
        testArmor.setType(ArmorType.PLATE);
        testArmor.setArmorPrimaryAttributes(new PrimaryAttributes(2,1,0,0));

        testWarrior.equipWeapon(testWeapon);
        testWarrior.equipArmor(testArmor);

        double expectedDPS =  (7 * 1.1) * (1 + ((5+1) / 100));

        assertEquals(expectedDPS, testWarrior.getDPS());
    }




}