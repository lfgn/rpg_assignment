package se.experis.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    @Test
    void character_starts_with_level1_isTrue(){
        Mage testMage = new Mage("testMage");
        int startLevel = testMage.getLevel();
        assertEquals(1, startLevel);
    }

    @Test
    void character_levellingUp_once_raiseLevel_to2_isTrue(){
        Ranger testRanger = new Ranger("testRanger");
        testRanger.levelUp();
        int levelRaisedOnce = testRanger.getLevel();
        assertEquals(2,levelRaisedOnce);
    }

    @Test
    void character_level_cannotBe_zeroOrLess(){
        Rogue testRogue = new Rogue("testRogue");
        assertThrows(IllegalArgumentException.class, ()-> testRogue.setLevel(0));
    }

    @Test
    void mageCharacter_created_with_right_attributes_isTrue(){
        Mage testMage = new Mage("testMage");
        StringBuilder attributesToString = new StringBuilder();
        attributesToString.append(" Vitality: "+testMage.getBasePrimaryAttributes().getVitality());
        attributesToString.append(" Strength: "+testMage.getBasePrimaryAttributes().getStrength());
        attributesToString.append(" Dexterity: "+testMage.getBasePrimaryAttributes().getDexterity());
        attributesToString.append(" Intelligence: "+testMage.getBasePrimaryAttributes().getIntelligence());

        String expectedAttributes = " Vitality: 5 Strength: 1 Dexterity: 1 Intelligence: 8";
        assertTrue(expectedAttributes.equals(attributesToString.toString()));
    }

    @Test
    void rangerCharacter_created_with_right_attributes_isTrue(){
        Ranger testRanger = new Ranger("testRanger");
        StringBuilder attributesToString = new StringBuilder();
        attributesToString.append(" Vitality: "+testRanger.getBasePrimaryAttributes().getVitality());
        attributesToString.append(" Strength: "+testRanger.getBasePrimaryAttributes().getStrength());
        attributesToString.append(" Dexterity: "+testRanger.getBasePrimaryAttributes().getDexterity());
        attributesToString.append(" Intelligence: "+testRanger.getBasePrimaryAttributes().getIntelligence());

        String expectedAttributes = " Vitality: 8 Strength: 1 Dexterity: 7 Intelligence: 1";
        assertTrue(expectedAttributes.equals(attributesToString.toString()));
    }

    @Test
    void rogueCharacter_created_with_right_attributes_isTrue(){
        Rogue testRogue = new Rogue("testRogue");
        StringBuilder attributesToString = new StringBuilder();
        attributesToString.append(" Vitality: "+testRogue.getBasePrimaryAttributes().getVitality());
        attributesToString.append(" Strength: "+testRogue.getBasePrimaryAttributes().getStrength());
        attributesToString.append(" Dexterity: "+testRogue.getBasePrimaryAttributes().getDexterity());
        attributesToString.append(" Intelligence: "+testRogue.getBasePrimaryAttributes().getIntelligence());

        String expectedAttributes = " Vitality: 8 Strength: 2 Dexterity: 6 Intelligence: 1";
        assertTrue(expectedAttributes.equals(attributesToString.toString()));
    }

    @Test
    void warriorCharacter_created_with_right_attributes_isTrue(){
        Warrior testWarrior = new Warrior("testRogue");
        StringBuilder attributesToString = new StringBuilder();
        attributesToString.append(" Vitality: "+testWarrior.getBasePrimaryAttributes().getVitality());
        attributesToString.append(" Strength: "+testWarrior.getBasePrimaryAttributes().getStrength());
        attributesToString.append(" Dexterity: "+testWarrior.getBasePrimaryAttributes().getDexterity());
        attributesToString.append(" Intelligence: "+testWarrior.getBasePrimaryAttributes().getIntelligence());

        String expectedAttributes = " Vitality: 10 Strength: 5 Dexterity: 2 Intelligence: 1";
        assertTrue(expectedAttributes.equals(attributesToString.toString()));
    }

    @Test
    void mageCharacter_levelledUp_with_right_attributes_isTrue(){
        Mage testMage = new Mage("testMage");
        testMage.levelUp();
        StringBuilder attributesToString = new StringBuilder();
        attributesToString.append(" Vitality: "+testMage.getBasePrimaryAttributes().getVitality());
        attributesToString.append(" Strength: "+testMage.getBasePrimaryAttributes().getStrength());
        attributesToString.append(" Dexterity: "+testMage.getBasePrimaryAttributes().getDexterity());
        attributesToString.append(" Intelligence: "+testMage.getBasePrimaryAttributes().getIntelligence());

        String expectedAttributes = " Vitality: 8 Strength: 2 Dexterity: 2 Intelligence: 13";
        assertTrue(expectedAttributes.equals(attributesToString.toString()));
    }

    @Test
    void rangerCharacter_levelledUp_with_right_attributes_isTrue(){
        Ranger testRanger = new Ranger("testRanger");
        testRanger.levelUp();
        StringBuilder attributesToString = new StringBuilder();
        attributesToString.append(" Vitality: "+testRanger.getBasePrimaryAttributes().getVitality());
        attributesToString.append(" Strength: "+testRanger.getBasePrimaryAttributes().getStrength());
        attributesToString.append(" Dexterity: "+testRanger.getBasePrimaryAttributes().getDexterity());
        attributesToString.append(" Intelligence: "+testRanger.getBasePrimaryAttributes().getIntelligence());

        String expectedAttributes = " Vitality: 10 Strength: 2 Dexterity: 12 Intelligence: 2";
        assertTrue(expectedAttributes.equals(attributesToString.toString()));
    }

    @Test
    void rogueCharacter_levelledUp_with_right_attributes_isTrue(){
        Rogue testRogue = new Rogue("testRogue");
        testRogue.levelUp();
        StringBuilder attributesToString = new StringBuilder();
        attributesToString.append(" Vitality: "+testRogue.getBasePrimaryAttributes().getVitality());
        attributesToString.append(" Strength: "+testRogue.getBasePrimaryAttributes().getStrength());
        attributesToString.append(" Dexterity: "+testRogue.getBasePrimaryAttributes().getDexterity());
        attributesToString.append(" Intelligence: "+testRogue.getBasePrimaryAttributes().getIntelligence());

        String expectedAttributes = " Vitality: 11 Strength: 3 Dexterity: 10 Intelligence: 2";
        assertTrue(expectedAttributes.equals(attributesToString.toString()));
    }

    @Test
    void warriorCharacter_levelledUp_with_right_attributes_isTrue(){
        Warrior testWarrior = new Warrior("testWarrior");
        testWarrior.levelUp();
        StringBuilder attributesToString = new StringBuilder();
        attributesToString.append(" Vitality: "+testWarrior.getBasePrimaryAttributes().getVitality());
        attributesToString.append(" Strength: "+testWarrior.getBasePrimaryAttributes().getStrength());
        attributesToString.append(" Dexterity: "+testWarrior.getBasePrimaryAttributes().getDexterity());
        attributesToString.append(" Intelligence: "+testWarrior.getBasePrimaryAttributes().getIntelligence());

        String expectedAttributes = " Vitality: 15 Strength: 8 Dexterity: 4 Intelligence: 2";
        assertTrue(expectedAttributes.equals(attributesToString.toString()));
    }

    @Test
    void warriorCharacter_levelledUp_with_right_secondaryAttributes_isTrue(){
        Warrior testWarrior = new Warrior("testWarrior");
        testWarrior.levelUp();
        StringBuilder attributesToString = new StringBuilder();
        attributesToString.append(" Health: "+testWarrior.getSecondaryAttributes().getHealth());
        attributesToString.append(" ArmorRating: "+testWarrior.getSecondaryAttributes().getArmorRating());
        attributesToString.append(" ElementalResistance: "+testWarrior.getSecondaryAttributes().getElementalResistance());

        String expectedAttributes = " Health: 150 ArmorRating: 12 ElementalResistance: 2";
        assertTrue(expectedAttributes.equals(attributesToString.toString()));
    }

}