package se.experis.items.types;

public enum WeaponType {
    AXE, BOW, DAGGER, HAMMER, STAFF, SWORD, WAND
}
