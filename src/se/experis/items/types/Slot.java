package se.experis.items.types;

public enum Slot {
    HEAD, BODY, LEGS, WEAPON
}
