package se.experis.items.types;

public enum ArmorType {
    CLOTH, LEATHER, MAIL, PLATE
}
