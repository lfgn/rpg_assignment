package se.experis.items;

import se.experis.items.types.Slot;
import se.experis.items.types.WeaponType;

public class Weapon extends Item {

    private WeaponType type;
    private double damage;
    private double attackSpeed;
    private double DPS;

    public Weapon(){
        this.setSlot(Slot.WEAPON);
    }

    public WeaponType getType() {
        return type;
    }

    public void setType(WeaponType type) {
        this.type = type;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public double getDPS() {
        return DPS;
    }

    public void setDPS(){
        this.DPS = this.attackSpeed * this.damage;
    }
}
