package se.experis.items;

import se.experis.attributes.PrimaryAttributes;
import se.experis.items.types.ArmorType;

public class Armor extends Item{

    private ArmorType type;
    private PrimaryAttributes armorPrimaryAttributes;

    public Armor(){}

    public ArmorType getType() {
        return type;
    }

    public void setType(ArmorType type) {
        this.type = type;
    }

    public PrimaryAttributes getArmorPrimaryAttributes() {
        return armorPrimaryAttributes;
    }

    public void setArmorPrimaryAttributes(PrimaryAttributes armorPrimaryAttributes) {
        this.armorPrimaryAttributes = armorPrimaryAttributes;
    }
}
