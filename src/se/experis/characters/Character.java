package se.experis.characters;

import se.experis.attributes.PrimaryAttributes;
import se.experis.attributes.SecondaryAttributes;
import se.experis.customExceptions.InvalidArmorException;
import se.experis.customExceptions.InvalidWeaponException;
import se.experis.items.Armor;
import se.experis.items.Item;
import se.experis.items.Weapon;
import se.experis.items.types.Slot;

import java.util.HashMap;

public abstract class Character {
    private String name;
    private int level;
    private PrimaryAttributes basePrimaryAttributes;
    private PrimaryAttributes totalPrimaryAttributes;
    private SecondaryAttributes secondaryAttributes;
    private final HashMap<Slot, Item> equipment = new HashMap<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    /**
     * Method that sets 'level' value for every character. The 'level' value must be bigger than zero.
     * @param level Character's level value that can be incremented and is useful to add new equipment when the level is appropriate.
     */
    public void setLevel(int level) {
        if(!(level <= 0)){
        this.level = level;
        }else throw new IllegalArgumentException("Character's level must be at least 1");
    }

    public PrimaryAttributes getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public void setBasePrimaryAttributes(PrimaryAttributes basePrimaryAttributes) {
        this.basePrimaryAttributes = basePrimaryAttributes;
    }

    public PrimaryAttributes getTotalPrimaryAttributes() {
        return totalPrimaryAttributes;
    }

    public void setTotalPrimaryAttributes(PrimaryAttributes totalPrimaryAttributes) {
        this.totalPrimaryAttributes = totalPrimaryAttributes;
    }

    public SecondaryAttributes getSecondaryAttributes() {
        return secondaryAttributes;
    }

    public void setSecondaryAttributes(SecondaryAttributes secondaryAttributes) {
        this.secondaryAttributes = secondaryAttributes;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    /**
     * Method implemented inside each character's 'equipArmor' method which adds up the armor's attributes together
     * with characters 'basePrimaryAttributes' in order to define character's 'totalPrimaryAttributes'. Once the 'totalPrimaryAttributes' are defined
     * the character's 'secondaryAttributes' are defined.
     * When both 'totalPrimaryAttributes' and 'secondaryAttributes' are defined then the 'Armor' objet is added to the character's 'equipment' HashMap List.
     *
     * @param armor 'Armor' object that is added into character's 'equipment' HashMap List.
     */
    public void equippingArmor(Armor armor){
        var existingAttributes = this.getBasePrimaryAttributes();
        var armorAttributes = armor.getArmorPrimaryAttributes();  //updating total attributes adding BasePrimaryAttributes and ArmorPrimaryAttributes
        this.setTotalPrimaryAttributes(new PrimaryAttributes(existingAttributes.getVitality() + armorAttributes.getVitality(), existingAttributes.getStrength() + armorAttributes.getStrength(), existingAttributes.getDexterity() +armorAttributes.getDexterity(), existingAttributes.getIntelligence() + armorAttributes.getIntelligence() ));

        var currentTotalAttributes = this.getTotalPrimaryAttributes(); //updating second attributes in relation with the new totalAttributes
        this.setSecondaryAttributes(new SecondaryAttributes(currentTotalAttributes.getVitality() * 10, currentTotalAttributes.getStrength() + currentTotalAttributes.getDexterity(), currentTotalAttributes.getIntelligence() * 1));
        equipment.put(armor.getSlot(), armor);
    }

    public abstract void levelUp();
    public abstract void equipWeapon(Weapon weapon) throws InvalidWeaponException;
    public abstract void equipArmor(Armor armor) throws InvalidArmorException;
    public abstract double getDPS();
    public abstract String displayStats();

}
