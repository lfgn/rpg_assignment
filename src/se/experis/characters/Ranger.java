package se.experis.characters;

import se.experis.attributes.PrimaryAttributes;
import se.experis.attributes.SecondaryAttributes;
import se.experis.customExceptions.InvalidArmorException;
import se.experis.customExceptions.InvalidWeaponException;
import se.experis.items.*;
import se.experis.items.types.ArmorType;
import se.experis.items.types.Slot;
import se.experis.items.types.WeaponType;


public class Ranger extends Character{

    /**
     * Constructor that initialize the 'Ranger character' object.
     * When the object is created the only parameter required is a String to define
     * character's name. The character's level is set as '1' by default, as well as the
     * 'primaryAttributes' and 'secondaryAttributes' defined according to character's
     * specification.
     *
     * @param name String value that defines character's name.
     */
    public Ranger(String name){
        this.setName(name);
        this.setLevel(1);
        this.setBasePrimaryAttributes(new PrimaryAttributes(8,1,7,1));
        this.setSecondaryAttributes(new SecondaryAttributes(this.getBasePrimaryAttributes().getVitality() * 10, this.getBasePrimaryAttributes().getDexterity() + this.getBasePrimaryAttributes().getStrength(), this.getBasePrimaryAttributes().getIntelligence()));
    }

    /**
     * Method that raises the character's 'level' value as well as the 'primaryAttributes' values
     * according to the character's levelling specification. Once the 'primaryAttributes' are defined,
     * the 'secondaryAttributes' are modified accordingly.
     */
    @Override
    public void levelUp() {
        this.setLevel(this.getLevel() + 1);
        this.setBasePrimaryAttributes(new PrimaryAttributes(this.getBasePrimaryAttributes().getVitality() + 2, this.getBasePrimaryAttributes().getStrength() + 1, this.getBasePrimaryAttributes().getDexterity() + 5, this.getBasePrimaryAttributes().getIntelligence() + 1));
        this.setSecondaryAttributes(new SecondaryAttributes(this.getBasePrimaryAttributes().getVitality() * 10, this.getBasePrimaryAttributes().getDexterity() + this.getBasePrimaryAttributes().getStrength(), this.getBasePrimaryAttributes().getIntelligence()));
    }

    /**
     * Method that verifies that the properties of the 'Weapon' object which is being
     * tried to add in the character's 'equipment' HashMap List are suitable for the
     * character's type according to the specification. If the features of the 'Weapon' object
     * are suitable then the object is added to the character's 'equipment' HashMap List. If features
     * do not suit a costumed 'InvalidWeaponException' is thrown.
     *
     * @param weapon 'Weapon' object intended to be added at current character's 'equipment HashMap List.
     * @throws InvalidWeaponException If character's level doesn't suits weapon's level, or if weapon's type
     * doesn't suit the current character an 'InvalidWeaponException' is thrown.
     */
    @Override
    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        var equipment = this.getEquipment();
        if(weapon.getType().equals(WeaponType.BOW)){
            if(weapon.getLevel() <= this.getLevel()){
            equipment.put(weapon.getSlot(),weapon);
            }else throw new InvalidWeaponException("Ranger's level is too low to equip this weapon");
        } else throw new InvalidWeaponException("Ranger can only equip 'BOW' weapon");
    }

    /**
     * Method that verifies that the properties of the 'Armor' object which is being
     * tried to add in the character's 'equipment' HashMap List are suitable for the
     * character's type according to the specification. If the features of the 'Armor' object
     * are suitable then the 'equippingArmor' method is called to add the 'Armor' object
     * into the character's 'equipment' HashMap List. If 'Armor' object's features
     * do not suit the current character's type a costumed 'InvalidArmorException' is thrown.
     *
     * @param armor 'Armor' object intended to be added at current character's 'equipment HashMap List.
     * @throws InvalidArmorException If character's level doesn't suits armor's level, or if armor's type
     *  doesn't suit the current character's type an 'InvalidArmorException' is thrown.
     */
    @Override
    public void equipArmor(Armor armor) throws InvalidArmorException {
        if(armor.getType().equals(ArmorType.LEATHER) || armor.getType().equals(ArmorType.MAIL)){
            if(armor.getLevel() <= this.getLevel()){
            this.equippingArmor(armor);
            } else throw new InvalidArmorException("Ranger's level is too low to equip this armor");
        }else throw new InvalidArmorException("Ranger can only equip with 'LEATHER' or 'MAIL' as armor");
    }

    /**
     * Method that returns character's 'damage per second' as a double value depending
     * if a 'Weapon' object exists already in the character's 'equipment' HashMap List,
     * if both a 'Weapon' and 'Armor' objects exist in character's equipment List, or if
     * no 'Weapon' object or 'Armor' object have been added to character's equipment HashMap List.
     * @return double value that represents character's 'damage per second'.
     */
    @Override
    public double getDPS(){
        var equipment = this.getEquipment();
        if(equipment.containsKey(Slot.WEAPON) && !(equipment.containsKey(Slot.BODY) || equipment.containsKey(Slot.HEAD) || equipment.containsKey(Slot.LEGS)) ){
            var weaponOnList = (Weapon)equipment.get(Slot.WEAPON);
            weaponOnList.setDPS();//
            double WDPS = weaponOnList.getDPS();
            return ((double)(this.getBasePrimaryAttributes().getDexterity() / 100)+ 1) * WDPS;
        }else if(equipment.containsKey(Slot.WEAPON) && (equipment.containsKey(Slot.BODY) || equipment.containsKey(Slot.HEAD) || equipment.containsKey(Slot.LEGS))){
            var weaponOnList = (Weapon)equipment.get(Slot.WEAPON);
            weaponOnList.setDPS();//
            double WDPS = weaponOnList.getDPS();
            return ((double)(this.getTotalPrimaryAttributes().getDexterity() / 100)+ 1) * WDPS;
        }
        else return ((double)(this.getBasePrimaryAttributes().getDexterity() /100)+ 1) * 1;
    }

    /**
     * Method that returns a String with the specification of the current character's statistics.
     *
     * @return String object with character's statistics.
     */
    @Override
    public String displayStats(){
        StringBuilder stats = new StringBuilder();
        if(this.getTotalPrimaryAttributes() != null){
            stats.append("Ranger's name: "+this.getName()+"\n");
            stats.append("Ranger's level: "+this.getLevel()+"\n");
            stats.append("Ranger's strength: "+this.getTotalPrimaryAttributes().getStrength()+"\n");
            stats.append("Ranger's dexterity: "+this.getTotalPrimaryAttributes().getDexterity()+"\n");
            stats.append("Ranger's intelligence: "+this.getTotalPrimaryAttributes().getIntelligence()+"\n");
            stats.append("Ranger's health: "+this.getSecondaryAttributes().getHealth()+"\n");
            stats.append("Ranger's armor rating: "+this.getSecondaryAttributes().getArmorRating()+"\n");
            stats.append("Ranger's elemental resistance: "+this.getSecondaryAttributes().getElementalResistance()+"\n");
            stats.append("Ranger's DPS: "+this.getDPS()+"\n");
        }else if(this.getTotalPrimaryAttributes() == null){
            stats.append("Ranger's name: "+this.getName()+"\n");
            stats.append("Ranger's level: "+this.getLevel()+"\n");
            stats.append("Ranger's strength: "+this.getBasePrimaryAttributes().getStrength()+"\n");
            stats.append("Ranger's dexterity: "+this.getBasePrimaryAttributes().getDexterity()+"\n");
            stats.append("Ranger's intelligence: "+this.getBasePrimaryAttributes().getIntelligence()+"\n");
            stats.append("Ranger's health: "+this.getSecondaryAttributes().getHealth()+"\n");
            stats.append("Ranger's armor rating: "+this.getSecondaryAttributes().getArmorRating()+"\n");
            stats.append("Ranger's elemental resistance: "+this.getSecondaryAttributes().getElementalResistance()+"\n");
            stats.append("Ranger's DPS: "+this.getDPS()+"\n");
        }
        return stats.toString();
    }
}
