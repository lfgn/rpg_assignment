package se.experis.customExceptions;

public class InvalidArmorException extends Exception{
    public InvalidArmorException(String errorMessage){
        super(errorMessage);
    }
}
